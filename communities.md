layout: default.liquid
title: Communita Locali
---

## Rust Roma

![Rust Roma](/logos/rust-roma-small.png)  
[Meetup](https://www.meetup.com/Rust-Roma/)  
[Github](https://github.com/RustRome)  
[Twitter](https://twitter.com/RustRome)  

## Rust Milano

![Rust Milano](/logos/rust-milano-small.png)  
[Meetup](https://www.meetup.com/rust-language-milano/)  
[Github]()  

## Rust Torino

[Meetup] (....)  
[Github]()  

