layout: default.liquid
title: Rust Italia
---
### Benvenuti nella community Italiana per Rust (lang).

# Riferimenti

### [slack channel](http://rust-italia.horokuapp.com)

### [Community Locali](communities.html)

### [Blog](blog.html)
